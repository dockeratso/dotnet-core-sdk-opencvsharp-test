using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenCvSharp;

namespace ImageTest
{
    [TestClass]
    public class BasicTest
    {
        [TestMethod]
        public void CreateDispose()
        {
            var width = 640;
            var height = 480;
            using var a = new Mat(height, width, MatType.CV_32F);
            a.SetTo(1.0);
            a.MinMaxLoc(out double minValue, out double maxValue);
        }
    }
}
